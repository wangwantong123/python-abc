import speech_recognition
import pyttsx3

recongnizer = speech_recognition.Recognizer()

while True:
    try:
        with speech_recognition.Microphone() as mic:
            recongnizer.adjust_for_ambient_noise(mic, duration=0.2)
            recongnizer.energy_threshold = 1932
            recongnizer.dynamic_energy_threshold = True
            recongnizer.pause_threshold = 1.2
            print("Say something!")
            audio = recongnizer.listen(mic)
            text = recongnizer.recognize_google(audio, language="zh-cn")
            #text = text.lower()
            print(f"Recognized {text}")
    except speech_recognition.UnknownValueError():
        print("Could not understand audio")
        recongnizer = speech_recognition.Recognizer()
        continue
    except speech_recognition.RequestError as e:
        print("Could not request results {0}".format(e))
        recongnizer = speech_recognition.Recognizer()
        continue