import face_recognition
import cv2

#video_capture = cv2.VideoCapture('./video/demo_clip.mp4')
video_capture = cv2.VideoCapture(0)

while True:
    ret, frame = video_capture.read()
    img_rgb = frame[:, :, ::-1]

    # face_location = face_recognition.face_locations(img_rgb)
    face_location = face_recognition.face_locations(img_rgb, model="cnn")# 使用CNN模型

    for (top, right, bottom, left) in face_location:
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)  # 绘制检测框
    cv2.imshow('image', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

video_capture.release()
cv2.destroyAllWindows()